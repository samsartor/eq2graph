# Eq2Graph

> *This project is a work-in-progress. There are major bugs and limitations!*

An interactive, constraint-based algebra assistant:
```
$ eq2graph
[in] a^2 = exp b
[in] solve a
a = root(exp b, 2)

[in] ln a^2 => 2 a
[in] solve a
a = exp(b / 2)

[in] dot

    2
     \
     mul --> b
     /
    exp --> a

[in]
```

## Running

1. For a complete build and install, run
`cargo install --path . --features minifb `
    - Eq2Graph will be installed in `.cargo/bin`
    - For a minimal build, use `cargo build`
2. Install [Graphviz](https://graphviz.org/download/). Make sure `neato` is on your PATH.
3. Clone and compile [the Glasgow Subgraph Solver](https://github.com/ciaranm/glasgow-subgraph-solver).
4. Run `eq2graph`
    - If glasgow_subgraph_solver is not on your PATH, use
    `eq2graph --glasgow PATH_TO_EXE`

## Theory

![Quadratic Formula](quadratic.png)

Eq2Graph makes a variable out of every input and output of every operation in
your equation. The operations then act as constraints on these variables. You
can see above an example of the constraint graph: where variables are edges and
operations are nodes.

Consider some effects of this representation:
- Operations no longer take given inputs and produce resulting outputs. Instead
  each abstractly limits what combinations of values could satisfy related
  variables.
- As a result, there is no need for inverse operators such as subtraction,
  division, or roots.
- By traversing the constraint graph to decide on inputs and outputs for each
  node, it can be converted back into an equation form.
- Substitution can be performed by finding and replacing any subgraphs
  isomorphic to a given pattern.

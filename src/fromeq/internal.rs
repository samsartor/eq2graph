use super::Driver;
use once_cell::sync::Lazy;
use pest::error::{Error, ErrorVariant};
use pest::prec_climber::PrecClimber;
use pest::{iterators, Span};

#[derive(pest_derive::Parser)]
#[grammar = "equations.pest"]
pub struct EquationsLexer;
pub use Rule as LexerRule;
type Pair<'a> = iterators::Pair<'a, LexerRule>;
type Pairs<'a> = iterators::Pairs<'a, LexerRule>;

pub struct Parser<'a, D: Driver<'a>> {
    pub driver: D,
    pub in_namespace: D::Ns,
}

impl<'a, D: Driver<'a>> Parser<'a, D> {
    pub fn new(mut driver: D) -> Self {
        Self {
            in_namespace: driver.namespace(None),
            driver,
        }
    }

    pub fn new_in(driver: D, namespace: D::Ns) -> Self {
        Self {
            in_namespace: namespace.clone(),
            driver,
        }
    }
}

static PREC: Lazy<PrecClimber<LexerRule>> = Lazy::new(|| {
    use pest::prec_climber::{Assoc::Left, Assoc::Right, Operator};
    use Rule::*;

    PrecClimber::new(vec![
        Operator::new(add, Left) | Operator::new(sub, Left),
        Operator::new(mul, Left) | Operator::new(div, Left),
        Operator::new(implicit_mul, Right),
        Operator::new(pow, Left),
    ])
});

pub enum ParserError<'a> {
    UndefinedFunction(&'a str, Span<'a>),
    DefinedFunction(&'a str, Span<'a>, bool),
    Unknown(anyhow::Error, Span<'a>),
}

impl<'a> ParserError<'a> {
    fn inside(mut self) -> Self {
        if let ParserError::DefinedFunction(_, _, ref mut direct) = self {
            *direct = false;
        }
        self
    }

    fn into(self) -> Error<LexerRule> {
        match self {
            ParserError::UndefinedFunction(name, span) => Error::new_from_span(
                ErrorVariant::CustomError {
                    message: format!("{} is not defined", name),
                },
                span,
            ),
            ParserError::DefinedFunction(name, span, _) => Error::new_from_span(
                ErrorVariant::CustomError {
                    message: format!("{} is a function", name),
                },
                span,
            ),
            ParserError::Unknown(err, span) => Error::new_from_span(
                ErrorVariant::CustomError {
                    message: err.to_string(),
                },
                span,
            ),
        }
    }
}

pub type ParserResult<'a, T> = Result<T, ParserError<'a>>;
pub type LexerResult<T> = Result<T, Error<LexerRule>>;

impl<'a, D: Driver<'a>> Parser<'a, D> {
    fn new_var(&mut self) -> D::Var {
        self.driver.variable(&self.in_namespace, None)
    }

    fn p_var(&mut self, pair: Pair<'a>) -> ParserResult<'a, D::Var> {
        let name = pair.as_str();
        if self.driver.operation(name).is_some() {
            Err(ParserError::DefinedFunction(name, pair.as_span(), true))
        } else {
            Ok(self.driver.variable(&self.in_namespace, Some(name)))
        }
    }

    fn p_num(&mut self, pair: Pair<'a>) -> D::Var {
        let var = self.new_var();
        let value: i32 = pair.as_str().trim_start_matches('+').parse().unwrap();
        self.driver.constant(&var, value);
        var
    }

    fn p_expr(&mut self, pairs: Pairs<'a>) -> ParserResult<'a, D::Var> {
        use std::cell::RefCell;
        let this = RefCell::new(self);

        PREC.climb(
            pairs,
            |pair| {
                let mut this = this.borrow_mut();
                match pair.as_rule() {
                    Rule::ident => this.p_var(pair),
                    Rule::int => Ok(this.p_num(pair)),
                    Rule::application => this.p_application(pair.into_inner()),
                    Rule::expr => this.p_expr(pair.into_inner()),
                    _ => panic!(),
                }
            },
            |lhs, op, rhs| {
                let rhs = rhs.map_err(ParserError::inside)?;

                let mut this = this.borrow_mut();
                let this = &mut *this;
                let out = this.new_var();
                let op = match op.as_rule() {
                    Rule::add => "+",
                    Rule::sub => "-",
                    Rule::mul => "*",
                    Rule::div => "/",
                    Rule::pow => "^",
                    Rule::implicit_mul => {
                        if let Err(ParserError::DefinedFunction(name, _, true)) = lhs {
                            let op = this.driver.operation(name).unwrap();
                            this.driver.node(&this.in_namespace, &op, [&out, &rhs]);
                            return Ok(out);
                        }

                        "*"
                    }
                    _ => panic!(),
                };
                let lhs = lhs.map_err(ParserError::inside)?;
                let op = this.driver.operation(op).unwrap();
                this.driver
                    .node(&this.in_namespace, &op, [&out, &lhs, &rhs]);
                Ok(out)
            },
        )
        .map_err(ParserError::inside)
    }

    fn p_application(&mut self, pairs: Pairs<'a>) -> ParserResult<'a, D::Var> {
        use std::iter::once;

        let mut op = None;
        let mut args = Vec::new();
        for pair in pairs {
            match pair.as_rule() {
                Rule::ident => op = Some(pair),
                Rule::expr => args.push(self.p_expr(pair.into_inner())?),
                _ => panic!(),
            }
        }

        let op = op.unwrap();
        let op = match self.driver.operation(op.as_str()) {
            Some(o) => o,
            None => return Err(ParserError::UndefinedFunction(op.as_str(), op.as_span())),
        };

        let out = self.new_var();
        self.driver
            .node(&self.in_namespace, &op, once(&out).chain(args.iter()));
        Ok(out)
    }

    fn p_equality(&mut self, a: Pairs<'a>, b: Pairs<'a>) -> ParserResult<'a, ()> {
        let a = self.p_expr(a)?;
        let b = self.p_expr(b)?;
        self.driver.merge(&b, &a);
        Ok(())
    }

    fn p_definition(&mut self, a: Pairs<'a>, b: Pairs<'a>) -> ParserResult<'a, ()> {
        use std::iter::once;

        fn extract_rule(r: Rule, pairs: Pairs) -> Pair {
            for pair in pairs {
                if pair.as_rule() == r {
                    return pair;
                } else {
                    panic!()
                }
            }
            panic!()
        }

        let new_ns = self.driver.namespace(None);
        // TODO: restore old namespace on error
        let old_ns = std::mem::replace(&mut self.in_namespace, new_ns);

        let mut op = None;
        let mut args = Vec::new();
        for pair in extract_rule(Rule::application, a).into_inner() {
            match pair.as_rule() {
                Rule::ident => op = Some(pair.as_str()),
                Rule::expr => args.push(self.p_var(extract_rule(Rule::ident, pair.into_inner()))?),
                _ => panic!(),
            }
        }

        let out = self.p_expr(b)?;
        let args = once(&out).chain(args.iter());
        self.driver.define(&self.in_namespace, op.unwrap(), args);

        self.in_namespace = old_ns;

        Ok(())
    }

    fn p_substitution(&mut self, a: Pairs<'a>, b: Pairs<'a>, s: Span<'a>) -> ParserResult<'a, ()> {
        let pat_ns = self.driver.namespace(None);
        let into_ns = self.driver.namespace(Some(&pat_ns));
        let old_ns = std::mem::replace(&mut self.in_namespace, pat_ns);
        let out_a = self.p_expr(a)?;
        self.in_namespace = into_ns.clone();
        let out_b = self.p_expr(b)?;
        self.in_namespace = old_ns;
        self.driver.merge(&out_b, &out_a);
        self.driver
            .substitute(&into_ns, &self.in_namespace)
            .map_err(|e| ParserError::Unknown(e, s))
    }

    fn p_joined(&mut self, pairs: Pairs<'a>) -> ParserResult<'a, ()> {
        let mut a = None;
        let mut b = None;
        let mut joiner_span = None;
        let mut joiner = None;
        for pair in pairs {
            match pair.as_rule() {
                Rule::expr if a.is_none() => {
                    a = Some(pair.into_inner());
                }
                Rule::expr if b.is_none() => {
                    b = Some(pair.into_inner());
                }
                Rule::joiner => {
                    joiner_span = Some(pair.as_span());
                    joiner = Some(pair.as_str());
                }
                _ => panic!(),
            }
        }

        let a = a.unwrap();
        let b = b.unwrap();
        match joiner.unwrap() {
            "=" => self.p_equality(a, b)?,
            ":=" => self.p_definition(a, b)?,
            "=>" => self.p_substitution(a, b, joiner_span.unwrap())?,
            _ => panic!(),
        }

        Ok(())
    }

    fn p_calculation(&mut self, pairs: Pairs<'a>) -> ParserResult<'a, ()> {
        for pair in pairs {
            match pair.as_rule() {
                Rule::joined => self.p_joined(pair.into_inner())?,
                Rule::EOI => (),
                _ => panic!(),
            }
        }

        Ok(())
    }

    pub fn parse_var(&mut self, input: &'a str) -> LexerResult<D::Var> {
        let pairs = <EquationsLexer as pest::Parser<_>>::parse(Rule::single_var, input)?;
        for pair in pairs {
            match pair.as_rule() {
                Rule::ident => return self.p_var(pair).map_err(ParserError::into),
                _ => (),
            }
        }
        panic!()
    }

    pub fn parse(&mut self, input: &'a str) -> LexerResult<()> {
        let pairs = <EquationsLexer as pest::Parser<_>>::parse(Rule::calculation, input)?;
        self.p_calculation(pairs).map_err(ParserError::into)
    }

    pub fn parse_all(&mut self, input: &'a str) -> LexerResult<()> {
        let pairs = <EquationsLexer as pest::Parser<_>>::parse(Rule::calculations, input)?;
        self.p_calculation(pairs).map_err(ParserError::into)
    }
}

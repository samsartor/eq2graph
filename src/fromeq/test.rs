use std::collections::HashMap;
use std::fmt::{self, Write};

pub struct TestDriver {
    next_ns: usize,
    next_var: usize,
    var_map: HashMap<String, usize>,
    pub output: Vec<String>,
}

impl TestDriver {
    pub fn new() -> Self {
        Self {
            next_ns: 0,
            next_var: 0,
            var_map: HashMap::new(),
            output: Vec::new(),
        }
    }
}

#[derive(Clone)]
pub struct Nsed<N, I> {
    ns: N,
    id: I,
}

impl<N: AsRef<[usize]>, I: fmt::Display> fmt::Display for Nsed<N, I> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.ns.as_ref() == &[0] {
            write!(f, "{}", self.id)
        } else {
            let mut ns = String::new();
            for id in self.ns.as_ref() {
                write!(ns, "{}:", id)?;
            }

            write!(f, "{}{}", ns, self.id)
        }
    }
}

pub type TestVar = Nsed<Vec<usize>, usize>;

impl<'a> super::Driver<'a> for TestDriver {
    type Var = TestVar;
    type Ns = Vec<usize>;
    type Op = String;

    fn namespace(&mut self, parent: Option<&Self::Ns>) -> Self::Ns {
        use std::iter::once;

        let ns = self.next_ns;
        self.next_ns += 1;
        parent
            .into_iter()
            .flatten()
            .copied()
            .chain(once(ns))
            .collect()
    }

    fn operation(&self, op: &'a str) -> Option<Self::Op> {
        const OPS: &[&str] = &[
            "+", "-", "*", "/", "^", "exp", "log", "sqrt", "sin", "cos", "tan", "min", "max",
            "binom",
        ];

        if OPS.contains(&op) {
            Some(op.to_owned())
        } else {
            None
        }
    }

    fn define<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &'a str,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) {
        let mut out = format!("{} :=", Nsed { ns, id: op });
        for arg in args {
            write!(out, " {}", arg).unwrap();
        }
        self.output.push(out);
    }

    fn variable(&mut self, ns: &Self::Ns, name: Option<&'a str>) -> Self::Var {
        let next = self.next_var;
        let var = if let Some(name) = name {
            if let Some(var) = self.var_map.get(name) {
                *var
            } else {
                self.var_map.insert(name.to_owned(), next);
                self.next_var += 1;
                next
            }
        } else {
            self.next_var += 1;
            next
        };

        TestVar {
            ns: ns.clone(),
            id: var,
        }
    }

    fn merge(&mut self, var: &Self::Var, into: &Self::Var) {
        self.output.push(format!("{} = {}", into, var));
    }

    fn node<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &Self::Op,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) {
        let mut out = Nsed { ns, id: op }.to_string();
        for arg in args {
            write!(out, " {}", arg).unwrap();
        }
        self.output.push(out);
    }

    fn constant(&mut self, var: &Self::Var, value: i32) {
        self.output.push(format!("{} == {}", var, value));
    }

    fn substitute(
        &mut self,
        replacement: &Self::Ns,
        _within: &Self::Ns,
    ) -> Result<(), anyhow::Error> {
        assert_eq!(replacement.len(), 2);
        self.output
            .push(format!("{} => {}", replacement[0], replacement[1],));
        Ok(())
    }
}

pub fn assert_parsed(eqs: &[&str], outputs: &[&str]) {
    let mut input = String::new();
    for &eq in eqs.iter() {
        input.push_str(eq);
        input.push('\n');
    }

    let mut parser = super::Parser::new(TestDriver::new());
    parser.parse_all(&input).unwrap();

    let mut got = parser.driver.output.iter().map(String::as_str);
    let mut expected = outputs.iter().copied();
    loop {
        match (got.next(), expected.next()) {
            (Some(a), Some(b)) => assert_eq!(a, b),
            (None, None) => break,
            (a, b) => assert_eq!(a, b),
        }
    }
}

pub fn assert_not_parsed(eqs: &[&str]) {
    let mut input = String::new();
    for &eq in eqs.iter() {
        input.push_str(eq);
        input.push('\n');
    }

    let mut parser = super::Parser::new(TestDriver::new());
    assert!(parser.parse_all(&input).is_err());
}

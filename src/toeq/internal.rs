use crate::graph::{Constant, Graph, NodeId, VarId};
use rand::prelude::{Rng, ThreadRng};
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::collections::hash_map::Entry;
use std::collections::{BinaryHeap, HashMap, HashSet};

// TODO: because feature(entry_insert) is unstable
fn entry_insert<K, V>(e: Entry<K, V>, v: V) {
    match e {
        Entry::Occupied(mut e) => {
            e.insert(v);
        }
        Entry::Vacant(e) => {
            e.insert(v);
        }
    }
}

struct NodeVisit<E> {
    priority: i32,
    depth: usize,
    tie_break: f32,
    node: NodeId,
    output: VarId,
    expr: Option<E>,
}

// This is the priority of choosing `output` for `node` vs other discovered options.
// Note that there is no wrong answer here, but one strategy might make nicer looking outputs than another.
impl<E> Ord for NodeVisit<E> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.priority
            .cmp(&other.priority)
            //.then(self.depth.cmp(&other.depth).reverse())
            .then(self.tie_break.partial_cmp(&other.tie_break).unwrap())
    }
}

impl<E> PartialOrd for NodeVisit<E> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<E> PartialEq for NodeVisit<E> {
    fn eq(&self, other: &Self) -> bool {
        self.priority == other.priority && self.depth == other.depth
    }
}

impl<E> Eq for NodeVisit<E> {}

enum VarState {
    // The variable has been used for inputs only
    Input,
    // The variable has been bound to one output
    Output(usize),
    // The variable should have an explicit equality. Often because it has
    // been bound to muliple multiple outputs.
    Labeled,
}

impl VarState {
    fn has_output(&self) -> bool {
        match self {
            VarState::Input => false,
            VarState::Output(_) => true,
            VarState::Labeled => true,
        }
    }
}

pub struct Printer<'g, D: super::Driver> {
    pub driver: D,
    pub graph: &'g Graph,
    pub rng: ThreadRng,
    var_states: HashMap<VarId, VarState>,
    visit_queue: BinaryHeap<NodeVisit<D::Expr>>,
    output_set: HashSet<NodeId>,
    output_order: Vec<NodeVisit<D::Expr>>,
}

impl<'g, D: super::Driver> Printer<'g, D> {
    pub fn new(graph: &'g Graph, driver: D) -> Self {
        Printer {
            driver,
            graph,
            rng: ThreadRng::default(),
            var_states: HashMap::new(),
            visit_queue: BinaryHeap::new(),
            output_order: Vec::new(),
            output_set: HashSet::new(),
        }
    }

    pub fn add_starting_point(&mut self, var: VarId) {
        let (var, id) = self.graph.ref_var(var);
        self.var_states.insert(id, VarState::Labeled);

        for node in var.connected() {
            self.visit_queue.push(NodeVisit {
                priority: -2, // Don't create additional equalities for this starting point unless absolutely needed
                depth: 0,
                tie_break: self.rng.gen(),
                node: node,
                output: id,
                expr: None,
            });
        }
    }

    pub fn print(mut self) {
        self.run_bfs_pass();
        self.run_output_pass();
    }

    fn run_bfs_pass(&mut self) {
        while let Some(visit) = self.visit_queue.pop() {
            if !self.output_set.insert(visit.node) {
                continue;
            }

            let output_ind = self.output_order.len();

            // Bind to the output variable
            let state = self.var_states.entry(visit.output);
            let mut already_has_output = false;
            if let Entry::Occupied(state) = &state {
                already_has_output = state.get().has_output();
            }

            if already_has_output {
                // The variable is already bound to one output,
                // must establish explicit equality between them
                entry_insert(state, VarState::Labeled);
            } else {
                // Bind this output
                entry_insert(state, VarState::Output(output_ind));
            }

            // If we are visiting a relation, we will need to traverse to attached relations
            if let NodeId::Rel(rid) = visit.node {
                let rel = self.graph.ref_rel(rid);
                // Visit through all connected variables
                for (var, vid) in rel.params(self.graph) {
                    if vid == visit.output {
                        // We came to this relation from var, no need to enqueue nodes
                    } else {
                        // The parameter was chosen as an input.
                        self.var_states.entry(vid).or_insert(VarState::Input);

                        // Variable may lead to unexplored nodes. All should use it as an output.
                        let nominated_out = self.rng.gen_range(0..var.connected().len());
                        for (i, node) in var.connected().enumerate() {
                            self.visit_queue.push(NodeVisit {
                                // Prioritize for choosing this variable as output.
                                priority: match (var.name.is_some(), i == nominated_out) {
                                    (false, true) => 2, // If unnamed, should have exactly one output role if possible
                                    (false, false) => -2, // Additional output roles would require explicit equality and made-up name
                                    (true, true) => 1, // If named, less important as output (wold be best as an input)
                                    (true, false) => -1, // But still not great to form an extra explicit equality
                                },
                                depth: visit.depth + 1,
                                tie_break: self.rng.gen(),
                                node: node,
                                output: vid,
                                expr: None,
                            });
                        }
                    }
                }
            }

            self.output_order.push(visit);
        }
    }

    fn run_output_pass(&mut self) {
        use NodeId::*;
        use VarState::*;

        for idx in (0..self.output_order.len()).rev() {
            let NodeVisit { node, output, .. } = self.output_order[idx];

            let expr = match node {
                Rel(rid) => {
                    let rel = self.graph.ref_rel(rid);

                    let mut output_pos = 0;
                    let mut inputs = Vec::with_capacity(rel.raw_params().len() - 1);

                    for (pos, (var, vid)) in rel.params(&self.graph).enumerate() {
                        if vid == output {
                            output_pos = pos;
                        } else {
                            let in_expr = match self.var_states[&vid] {
                                Input | Labeled => self.driver.variable(vid, var.name()),
                                Output(out) => self.output_order[out]
                                    .expr
                                    .clone()
                                    .expect("outputs are out of order"),
                            };
                            inputs.push(in_expr);
                        }
                    }

                    match rel.operation {
                        Rel(_) => todo!(),
                        Const(cid) => {
                            let con = self.graph.ref_const(cid);
                            let op = match super::Operation::from_const(con, output_pos) {
                                Some(o) => o,
                                None => panic!(
                                    "could not print {:?} with output at index {}",
                                    con, output_pos
                                ),
                            };
                            self.driver.operation(op, inputs)
                        }
                    }
                }
                Const(cid) => {
                    let con = self.graph.ref_const(cid);
                    if let Constant::Int(val) = con {
                        self.driver.constant(val)
                    } else {
                        panic!("can not print {:?} as a constant", con)
                    }
                }
            };

            let (out_var, output2) = self.graph.ref_var(output);
            assert_eq!(output, output2);
            match self.var_states[&output] {
                Input => unreachable!(),
                Output(idx2) => {
                    assert_eq!(idx, idx2);
                    self.output_order[idx].expr = Some(expr)
                }
                Labeled => {
                    let var_expr = self.driver.variable(output, out_var.name());
                    self.driver.equality(var_expr, expr, false)
                }
            }
        }
    }
}

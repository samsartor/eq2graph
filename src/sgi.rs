use crate::graph::{ConstId, Graph, NsId, RelId, VarId};
use anyhow::{anyhow, bail, Context, Error};
use std::ffi::OsString;
use std::io::{self, BufRead, BufWriter};
use std::process::{Command, Stdio};
use tempfile::NamedTempFile;

#[derive(Default, Debug, Clone)]
pub struct Mapping {
    pub rels: Vec<(RelId, RelId)>,
    pub vars: Vec<(VarId, VarId)>,
    pub consts: Vec<(ConstId, ConstId)>,
}

pub trait SgiSolver {
    fn find(&self, graph: &Graph, pat_ns: NsId, tar_ns: NsId) -> Result<Vec<Mapping>, Error>;

    fn clone_dyn(&self) -> Box<dyn SgiSolver>;
}

#[derive(Clone)]
pub struct GlasgowSolver {
    pub command: OsString,
}

impl Default for GlasgowSolver {
    fn default() -> Self {
        GlasgowSolver {
            command: "glasgow_subgraph_solver".into(),
        }
    }
}

impl SgiSolver for GlasgowSolver {
    fn find(&self, graph: &Graph, pat_ns: NsId, tar_ns: NsId) -> Result<Vec<Mapping>, Error> {
        let mut pat_file = NamedTempFile::new()?;
        graph.print_csv(
            BufWriter::new(&mut pat_file),
            |_, var| var.namespace == pat_ns,
            |_, rel| rel.namespace == pat_ns,
            |cid, _| graph.const_in(pat_ns, cid),
        )?;
        let pat_file = pat_file.into_temp_path();

        let mut tar_file = NamedTempFile::new()?;
        graph.print_csv(
            BufWriter::new(&mut tar_file),
            |_, var| var.namespace == tar_ns,
            |_, rel| rel.namespace == tar_ns,
            |cid, _| graph.const_in(tar_ns, cid),
        )?;
        let tar_file = tar_file.into_temp_path();

        let mut gss = Command::new(&self.command)
            .arg(&pat_file)
            .arg(&tar_file)
            .arg("--print-all-solutions")
            .arg("--format")
            .arg("csv")
            .stdout(Stdio::piped())
            .spawn()
            .context("could not start glasgow_subgraph_solver")?;
        let stderr = io::stderr();
        let output = io::BufReader::new(tee::TeeReader::new(
            gss.stdout.take().unwrap(),
            stderr.lock(),
        ));

        let mut maps = Vec::new();
        for line in output.lines() {
            let mut map = Mapping::default();

            let line = line?;
            let line = match line.strip_prefix("mapping = ") {
                Some(l) => l,
                None => continue,
            };
            for mapping in line.trim().split(") (").filter(|s| !s.is_empty()) {
                let (left, right) = mapping
                    .trim_start_matches('(')
                    .trim_end_matches(')')
                    .split_once(" -> ")
                    .ok_or_else(|| {
                        anyhow!(
                            "invalid output from glasgow solver: missing -> in {:?}",
                            mapping
                        )
                    })?;
                let (a_ty, a) = left.split_at(1);
                let a: usize = a.parse()?;
                let (b_ty, b) = right.split_at(1);
                let b: usize = b.parse()?;
                if a_ty != b_ty {
                    bail!(
                        "invalid output from glasgow solver: matched dots have different types in {:?}",
                        mapping
                    )
                }

                match a_ty {
                    "v" => map.vars.push((a, b)),
                    "r" => map.rels.push((a, b)),
                    "c" => map.consts.push((a, b)),
                    _ => bail!(
                        "invalid output from glasgow solver: unknown type {:?} in {:?}",
                        a_ty,
                        mapping
                    ),
                }
            }

            maps.push(map);
        }

        Ok(maps)
    }

    fn clone_dyn(&self) -> Box<dyn SgiSolver> {
        Box::new(self.clone())
    }
}

impl Default for Box<dyn SgiSolver> {
    fn default() -> Self {
        Box::new(GlasgowSolver::default())
    }
}

impl Clone for Box<dyn SgiSolver> {
    fn clone(&self) -> Self {
        self.clone_dyn()
    }
}

use super::norm::{BiRelationEditor, RelationEditor};
use super::IdAlloc;
use crate::sgi::SgiSolver;
use std::collections::{BTreeSet, HashMap, HashSet};

pub type RelId = usize;
pub type ConstId = usize;

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub enum NodeId {
    Rel(RelId),
    Const(ConstId),
}

pub type VarId = usize;
pub type NsId = usize;

#[derive(Clone, Debug)]
pub struct Rel {
    pub operation: NodeId,
    pub namespace: NsId,
    params: Vec<VarId>,
}

impl Rel {
    pub fn const_op<'s>(&self, graph: &'s Graph) -> Option<&'s Constant> {
        match self.operation {
            NodeId::Rel(_) => None,
            NodeId::Const(id) => Some(graph.ref_const(id)),
        }
    }

    pub fn raw_params(&self) -> &[VarId] {
        self.params.as_slice()
    }

    pub fn params<'s>(&'s self, graph: &'s Graph) -> impl Iterator<Item = (&'s Var, VarId)> + '_ {
        self.params.iter().map(move |&id| graph.ref_var(id))
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Constant {
    Int(i32),
    Add,
    Mul,
    Pow,
    Sq { signed: bool },
    Exp,
    Relate,
}

#[derive(Clone, Debug)]
pub enum Node {
    Rel(Rel),
    Constant(Constant),
}

#[derive(Clone, Default, Debug)]
pub struct Var {
    redirect: Option<VarId>,
    connected_nodes: Vec<(NodeId, usize)>,
    pub name: Option<String>,
    pub namespace: NsId,
}

impl Var {
    pub fn name(&self) -> Option<&str> {
        self.name.as_ref().map(String::as_str)
    }

    pub fn connected(&self) -> impl ExactSizeIterator<Item = NodeId> + '_ {
        self.connected_nodes.iter().map(|&(i, _)| i)
    }

    pub fn connected_to(&self) -> impl ExactSizeIterator<Item = (NodeId, usize)> + '_ {
        self.connected_nodes.iter().copied()
    }
}

#[derive(Clone, Default, Debug)]
pub struct Namespace {
    vars: BTreeSet<VarId>,
    nodes: BTreeSet<NodeId>,
    parent: Option<NsId>,
}

impl Namespace {
    pub fn parent(&self) -> Option<NsId> {
        self.parent.clone()
    }

    pub fn nodes(&self) -> impl ExactSizeIterator<Item = NodeId> + '_ {
        self.nodes.iter().copied()
    }

    pub fn vars(&self) -> impl ExactSizeIterator<Item = VarId> + '_ {
        self.vars.iter().copied()
    }
}

#[derive(Clone, Default)]
pub struct Graph {
    var_ids: IdAlloc,
    rel_ids: IdAlloc,
    const_ids: IdAlloc,
    rels: HashMap<RelId, Rel>,
    consts: HashMap<ConstId, Constant>,
    vars: HashMap<VarId, Var>,
    namespaces: Vec<Namespace>,
    name_lookup: HashMap<(NsId, String), VarId>,
    def_lookup: HashMap<String, NodeId>,
    pub sgi_solver: Box<dyn SgiSolver>,
}

#[derive(Debug)]
pub struct NoSuchOp;

impl Graph {
    pub fn define_constant(&mut self, name: &str, value: Constant) -> ConstId {
        use std::collections::hash_map::Entry;

        match self.def_lookup.entry(name.to_owned()) {
            Entry::Occupied(_) => panic!("already inserted"),
            Entry::Vacant(e) => {
                let id = self.const_ids.next();
                self.consts.insert(id, value);
                e.insert(NodeId::Const(id));
                id
            }
        }
    }

    pub fn is_defined(&self, name: &str) -> bool {
        self.def_lookup.contains_key(name)
    }

    pub fn set_constant(&mut self, var: VarId, value: Constant) -> ConstId {
        let id = self.const_ids.next();
        self.consts.insert(id, value);
        let var = self.mut_var(var).0;
        var.connected_nodes.push((NodeId::Const(id), 0));
        let namespace = var.namespace;
        self.namespaces[namespace].nodes.insert(NodeId::Const(id));
        id
    }

    pub fn define_defaults(&mut self) {
        self.define_constant("+", Constant::Add);
        self.define_constant("*", Constant::Mul);
        self.define_constant("^", Constant::Pow);
        self.define_constant("usq", Constant::Sq { signed: false });
        self.define_constant("ssq", Constant::Sq { signed: true });
        self.define_constant("exp", Constant::Exp);
        self.define_constant("rel", Constant::Relate);
    }

    pub fn namespace(&mut self, parent: Option<NsId>) -> NsId {
        let id = self.namespaces.len();
        self.namespaces.push(Namespace {
            vars: BTreeSet::new(),
            nodes: BTreeSet::new(),
            parent,
        });
        id
    }

    pub fn def(&mut self, ns: NsId, name: &str, params: Vec<VarId>) {
        // A bit confusing: the def parameters are realated to each-other by the meta-relation "rel",
        // which is then be used as an operator node in other relations.
        let rel = self.rel(ns, "rel", params).expect("rel not defined");
        self.def_lookup.insert(name.to_owned(), NodeId::Rel(rel));
    }

    pub fn rel_with_node(
        &mut self,
        namespace: NsId,
        operation: NodeId,
        params: Vec<VarId>,
    ) -> RelId {
        let id = self.rel_ids.next();
        self.namespaces[namespace].nodes.insert(NodeId::Rel(id));
        for (pos, &var) in params.iter().enumerate() {
            self.mut_var(var)
                .0
                .connected_nodes
                .push((NodeId::Rel(id), pos));
        }
        self.rels.insert(
            id,
            Rel {
                namespace,
                operation,
                params,
            },
        );
        id
    }

    pub fn rel(&mut self, ns: NsId, op: &str, mut params: Vec<VarId>) -> Result<RelId, NoSuchOp> {
        let mut replace = None;
        RelationEditor {
            op: &op,
            params: &mut params,
            replace: &mut replace,
        }
        .dealias();

        let op = *self.def_lookup.get(replace.unwrap_or(op)).ok_or(NoSuchOp)?;
        Ok(self.rel_with_node(ns, op, params))
    }

    pub fn var(&mut self, ns: NsId, name: Option<&str>) -> VarId {
        let mut key = name.map(|name| (ns, name.to_string()));
        if let Some(key) = &mut key {
            loop {
                if let Some(&id) = self.name_lookup.get(key) {
                    return id;
                }

                match self.namespaces[ns].parent {
                    Some(ns) => key.0 = ns,
                    None => break,
                }
            }
        }

        let id = self.var_ids.next();
        if let Some((_, name)) = key {
            self.name_lookup.insert((ns, name), id);
        }
        self.vars.insert(
            id,
            Var {
                redirect: None,
                connected_nodes: Vec::new(),
                name: name.map(str::to_string),
                namespace: ns,
            },
        );
        self.namespaces[ns].vars.insert(id);
        id
    }

    pub fn merge(&mut self, source_id: VarId, dest_id: VarId) {
        let (_, dest_id) = self.ref_var(dest_id);
        let source = self.vars.get_mut(&source_id).unwrap();
        let mut new_connections = std::mem::replace(&mut source.connected_nodes, Vec::new());
        source.redirect = Some(dest_id);
        let name = source.name.clone();

        let dest = self.vars.get_mut(&dest_id).unwrap();
        dest.name = dest.name.take().or(name);
        dest.connected_nodes.append(&mut new_connections);
    }
}

impl Graph {
    pub fn mut_var(&mut self, var: VarId) -> (&mut Var, VarId) {
        // TODO: can be simplified once polotius is merged
        let (_, id) = self.ref_var(var);
        (self.vars.get_mut(&id).unwrap(), id)
    }

    pub fn ref_var(&self, mut var: VarId) -> (&Var, VarId) {
        loop {
            let found = &self.vars[&var];
            match found.redirect {
                None => return (found, var),
                Some(id) => var = id,
            }
        }
    }

    pub fn iter_vars(&self) -> impl Iterator<Item = (VarId, &Var)> {
        self.vars.iter().filter_map(|(&a, b)| {
            if b.redirect.is_some() {
                None
            } else {
                Some((a, b))
            }
        })
    }

    pub fn ref_const(&self, id: ConstId) -> &Constant {
        &self.consts[&id]
    }

    pub fn iter_consts(&self) -> impl Iterator<Item = (ConstId, &Constant)> {
        self.consts.iter().map(|(&a, b)| (a, b))
    }

    pub fn ref_rel(&self, id: RelId) -> &Rel {
        &self.rels[&id]
    }

    pub fn iter_rels(&self) -> impl Iterator<Item = (RelId, &Rel)> {
        self.rels.iter().map(|(&a, b)| (a, b))
    }

    pub fn ref_namespace(&self, id: NsId) -> &Namespace {
        &self.namespaces[id]
    }

    pub fn const_in(&self, ns: NsId, cid: ConstId) -> bool {
        // TODO: keep namespace in constant?
        self.namespaces[ns].nodes.contains(&NodeId::Const(cid))
    }
}

impl Graph {
    pub fn remove_nodes(&mut self, rm: &HashSet<NodeId>) {
        use NodeId::*;

        for id in rm {
            match id {
                Rel(rid) => {
                    self.rels.remove(rid);
                }
                Const(cid) => {
                    self.consts.remove(cid);
                }
            }
        }

        for var in self.vars.values_mut() {
            var.connected_nodes.retain(|(id, _)| !rm.contains(id));
        }
    }

    pub fn copy_from_ns(&mut self, source: NsId, dest: NsId, mut mapping: HashMap<VarId, VarId>) {
        use std::collections::hash_map::Entry;

        // Create all the new variables and make sure they are in the mapping
        let mut to_insert = Vec::new();
        for (&vid, var) in self.vars.iter() {
            if var.redirect.is_some() || var.namespace != source {
                continue;
            }

            let var_ids = &mut self.var_ids;
            if let Entry::Vacant(e) = mapping.entry(vid) {
                let new_id = var_ids.next();
                e.insert(new_id);
                to_insert.push((
                    new_id,
                    Var {
                        redirect: None,
                        connected_nodes: Vec::new(),
                        name: None,
                        namespace: dest,
                    },
                ));
            }
        }
        self.vars.extend(to_insert);

        // Copy and insert new constants
        let mut consts = Vec::new();
        for (from, &to) in mapping.iter() {
            for (nid, _) in &self.vars[from].connected_nodes {
                if let NodeId::Const(cid) = nid {
                    if self.const_in(source, *cid) {
                        consts.push(self.consts[cid].clone());
                    }
                }
            }

            for con in consts.drain(..) {
                self.set_constant(to, con);
            }
        }

        // Copy relations
        let mut to_insert = Vec::new();
        for rel in self.rels.values().filter(|rel| rel.namespace == source) {
            let params = rel
                .params(self)
                .map(|(_, vid)| mapping.get(&vid).copied().unwrap_or(vid))
                .collect();
            to_insert.push((rel.operation, params));
        }

        // Insert new relations
        for (op, params) in to_insert {
            self.rel_with_node(dest, op, params);
        }
    }

    fn simplify_individuals(&mut self) {
        for rel in self.rels.values_mut() {
            let op = match rel.operation {
                NodeId::Rel(_) => continue,
                NodeId::Const(cid) => &self.consts[&cid],
            };
            let mut replace = None;
            RelationEditor {
                op: op,
                params: &mut rel.params,
                replace: &mut replace,
            }
            .simplify();

            if let Some(op) = replace {
                rel.operation = self.def_lookup[op];
            }
        }
    }

    fn simplify_pairs(&mut self) {
        #[derive(Clone, Copy)]
        enum Target {
            Zero,
            One(RelId),
            Many,
        }

        use Target::*;

        impl Target {
            fn push(&mut self, nid: usize) {
                *self = match *self {
                    Zero => One(nid),
                    One(_) => Many,
                    Many => Many,
                }
            }
        }

        impl Default for Target {
            fn default() -> Self {
                Zero
            }
        }

        let mut edges: HashMap<VarId, (Target, Target)> = HashMap::new();
        for (&rid, rel) in self.rels.iter() {
            if let Some((_, vid)) = rel.params(self).next() {
                // "outputs" into variable
                edges.entry(vid).or_default().0.push(rid);
            }
            for (_, vid) in rel.params(self).skip(1) {
                // "inputs" from variable
                edges.entry(vid).or_default().1.push(rid);
            }
        }

        let mut to_remove = BTreeSet::new();
        for (_, (aid, bid)) in edges {
            let aid = if let One(i) = aid { i } else { continue };
            let a = &self.rels[&aid];
            let aop = if let Some(o) = a.const_op(self) {
                o
            } else {
                continue;
            };

            let bid = if let One(i) = bid { i } else { continue };
            let b = &self.rels[&bid];
            let bop = if let Some(o) = b.const_op(self) {
                o
            } else {
                continue;
            };

            if to_remove.contains(&aid) || to_remove.contains(&bid) {
                // TODO: repair edges map instead
                continue;
            }

            let namespace = a.namespace;
            let mut replace = None;
            BiRelationEditor {
                op_0: aop,
                op_1: bop,
                params_0: a.params.as_slice(),
                params_1: b.params.as_slice(),
                replace: &mut replace,
            }
            .simplify();

            if let Some((op, params)) = replace {
                to_remove.insert(aid);
                to_remove.insert(bid);
                self.rel(namespace, op, params).unwrap();
            }
        }

        for rid in to_remove {
            if let Some(rel) = self.rels.remove(&rid) {
                let nid = NodeId::Rel(rid);
                for vid in rel.params {
                    self.mut_var(vid)
                        .0
                        .connected_nodes
                        .retain(|&(n, _)| n != nid);
                }
            }
        }
    }

    pub fn garbage_collect_vars(&mut self) {
        self.vars
            .retain(|_, var| !var.connected_nodes.is_empty() || var.redirect.is_some());
    }

    pub fn simplify(&mut self) {
        self.simplify_individuals();
        self.simplify_pairs();
        self.garbage_collect_vars();
    }
}

use super::Constant::{self, *};
use super::VarId;

pub struct RelationEditor<'a, O> {
    pub op: &'a O,
    pub params: &'a mut Vec<VarId>,
    pub replace: &'a mut Option<&'static str>,
}

impl<'a, O> RelationEditor<'a, O> {
    pub fn set(&mut self, op: &'static str, params: impl IntoIterator<Item = VarId>) {
        *self.replace = Some(op);
        self.params.clear();
        self.params.extend(params);
    }
}

pub const SUPPORTED_ALIASES: &[&str] = &["-", "/", "ssqrt", "sq", "usqrt", "sqrt", "ln"];

impl<'a, 'b> RelationEditor<'a, &'b str> {
    pub fn dealias(mut self) {
        match (*self.op, self.params.as_slice()) {
            // o = a - b -> a = o + b
            ("-", &[o, a, b]) => self.set("+", [a, o, b]),
            // o = a / b -> a = o * b
            ("/", &[o, a, b]) => self.set("*", [a, o, b]),
            // o = +/- sqrt(i) -> i = o^2
            ("ssqrt", &[o, i]) => self.set("ssq", [i, o]),
            // alias sq with ssq
            ("sq", &[o, i]) => self.set("ssq", [o, i]),
            // o = sqrt(i) -> i = sq(o) where o > 0
            ("usqrt" | "sqrt", &[o, i]) => self.set("usq", [i, o]),
            // o = ln i -> i = exp(o)
            ("ln", &[o, i]) => self.set("exp", [i, o]),
            _ => (),
        }
    }
}

impl<'a> RelationEditor<'a, Constant> {
    pub fn simplify(mut self) {
        match (&*self.op, self.params.as_slice()) {
            // o = i * i -> o = sq(i)
            (Mul, &[o, a, b]) if a == b => self.set("ssq", [o, a]),
            _ => (),
        }
    }
}

pub struct BiRelationEditor<'a, O> {
    pub op_0: &'a O,
    pub op_1: &'a O,
    pub params_0: &'a [VarId],
    pub params_1: &'a [VarId],
    pub replace: &'a mut Option<(&'static str, Vec<VarId>)>,
}

impl<'a, O: Eq + Clone> BiRelationEditor<'a, O> {
    pub fn input_0(&self) -> impl Iterator<Item = VarId> + 'a {
        self.params_0.iter().copied().skip(1)
    }

    pub fn middle(&self) -> VarId {
        self.params_0[0]
    }

    pub fn input_1(&self) -> impl Iterator<Item = VarId> + 'a {
        let middle = self.middle();
        self.params_1
            .iter()
            .copied()
            .skip(1)
            .filter(move |&i| i != middle)
    }

    pub fn last(&self) -> Option<VarId> {
        self.params_1.get(0).copied()
    }

    pub fn all_input(&self) -> impl Iterator<Item = VarId> + 'a {
        self.input_0().chain(self.input_1())
    }

    pub fn replace(
        &mut self,
        op: &'static str,
        output: Option<VarId>,
        args: impl Iterator<Item = VarId>,
    ) {
        let args = output.into_iter().chain(args).collect();
        *self.replace = Some((op, args));
    }
}

impl<'a> BiRelationEditor<'a, Constant> {
    pub fn simplify(mut self) {
        match (self.op_0, self.op_1) {
            // (a + b) + c -> +(a b c)
            (Add, Add) => self.replace("+", self.last(), self.all_input()),
            // (a * b) * c -> *(a b c)
            (Mul, Mul) => self.replace("*", self.last(), self.all_input()),
            _ => (),
        }
    }
}

use super::{Constant, Graph, NodeId};
use std::collections::HashSet;
use std::fmt;
use std::io::{self, Error, Write};

impl Constant {
    pub fn print(&self, mut out: impl fmt::Write) -> Result<(), fmt::Error> {
        use Constant::*;

        match self {
            Int(i) => write!(out, "{}", i),
            Add => write!(out, "+"),
            Mul => write!(out, "*"),
            Pow => write!(out, "^"),
            Sq { signed: true } => write!(out, "ssq"),
            Sq { signed: false } => write!(out, "usq"),
            Exp => write!(out, "exp"),
            Relate => write!(out, "λ"),
        }
    }
}

impl Graph {
    pub fn print_graphvis(&self, mut out: impl Write) -> Result<(), Error> {
        writeln!(out, "graph {{")?;

        let mut found_consts = HashSet::new();
        for (id, var) in self.iter_vars() {
            if let Some(name) = &var.name {
                writeln!(out, "v{} [shape=plain,label={:?}];", id, name)?;
            } else {
                writeln!(out, "v{} [shape=point];", id)?;
            }

            for node in var.connected() {
                if let NodeId::Const(cid) = node {
                    if found_consts.insert(cid) {
                        let mut label = String::new();
                        let _ = self.ref_const(cid).print(&mut label);
                        writeln!(out, "c{} [label={:?}];", cid, label)?;
                    }
                    writeln!(out, "v{} -- c{};", id, cid)?;
                }
            }
        }

        for (id, rel) in self.iter_rels() {
            match rel.operation {
                NodeId::Rel(rid) => {
                    writeln!(out, "r{} [label=\"\"];", id)?;
                    writeln!(out, "r{} -- r{} [dir=forward];", rid, id)?;
                }
                NodeId::Const(cid) => {
                    let mut label = String::new();
                    let _ = self.ref_const(cid).print(&mut label);
                    writeln!(out, "r{} [label={:?}];", id, label)?;
                }
            }

            for (pos, (_, vid)) in rel.params(self).enumerate() {
                if pos == 0 {
                    writeln!(out, "r{} -- v{} [dir=forward];", id, vid)?;
                } else {
                    writeln!(out, "r{} -- v{};", id, vid)?;
                }
            }
        }

        writeln!(out, "}}")?;

        Ok(())
    }

    pub fn print_csv(
        &self,
        mut out: impl io::Write,
        filter_vars: impl Fn(super::VarId, &super::Var) -> bool,
        filter_rels: impl Fn(super::RelId, &super::Rel) -> bool,
        filter_cons: impl Fn(super::ConstId, &super::Constant) -> bool,
    ) -> Result<(), io::Error> {
        let mut rels_to_connect = HashSet::new();
        for (rid, rel) in self.iter_rels() {
            if !filter_rels(rid, rel) {
                continue;
            }

            rels_to_connect.insert(rid);

            if let Some(op) = rel.const_op(&self) {
                let mut op_str = String::new();
                let _ = op.print(&mut op_str);
                writeln!(out, "r{},,{}", rid, op_str)?;
            }
        }

        for (vid, var) in self.iter_vars() {
            if !filter_vars(vid, var) {
                continue;
            }

            writeln!(out, "v{},,_V", vid)?;

            for (rid, pos) in var.connected_to() {
                let (pre, num) = match rid {
                    NodeId::Rel(i) if rels_to_connect.contains(&i) => ('r', i),
                    NodeId::Rel(_) => continue,
                    NodeId::Const(i) => {
                        let con = self.ref_const(i);
                        if !filter_cons(i, con) {
                            continue;
                        }
                        let mut con_str = String::new();
                        let _ = con.print(&mut con_str);
                        writeln!(out, "c{},,{}", i, con_str)?;
                        ('c', i)
                    }
                };
                writeln!(out, "{}{},v{},{}", pre, num, vid, pos)?;
            }
        }

        Ok(())
    }
}

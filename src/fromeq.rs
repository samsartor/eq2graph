mod internal;
mod test;

pub use internal::{LexerRule, Parser};
pub use test::{assert_not_parsed, assert_parsed, TestDriver};

pub trait Driver<'a> {
    type Var: Clone + 'a;
    type Ns: Clone + 'a;
    type Op: Clone + 'a;

    fn namespace(&mut self, parent: Option<&Self::Ns>) -> Self::Ns;

    fn operation(&self, op: &'a str) -> Option<Self::Op>;
    fn define<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &'a str,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b;

    fn variable(&mut self, ns: &Self::Ns, name: Option<&'a str>) -> Self::Var;
    fn merge(&mut self, var: &Self::Var, into: &Self::Var);
    fn node<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &Self::Op,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b;
    fn constant(&mut self, var: &Self::Var, value: i32);

    fn substitute(
        &mut self,
        replacement: &Self::Ns,
        within: &Self::Ns,
    ) -> Result<(), anyhow::Error>;
}

impl<'r, 'a, D: Driver<'a>> Driver<'a> for &'r mut D {
    type Var = D::Var;
    type Ns = D::Ns;
    type Op = D::Op;

    fn namespace(&mut self, parent: Option<&Self::Ns>) -> Self::Ns {
        D::namespace(*self, parent)
    }

    fn operation(&self, op: &'a str) -> Option<Self::Op> {
        D::operation(*self, op)
    }

    fn define<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &'a str,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b,
    {
        D::define(*self, ns, op, args)
    }

    fn variable(&mut self, ns: &Self::Ns, name: Option<&'a str>) -> Self::Var {
        D::variable(*self, ns, name)
    }

    fn merge(&mut self, var: &Self::Var, into: &Self::Var) {
        D::merge(*self, var, into)
    }

    fn node<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &Self::Op,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b,
    {
        D::node(*self, ns, op, args)
    }

    fn constant(&mut self, var: &Self::Var, value: i32) {
        D::constant(*self, var, value)
    }

    fn substitute(
        &mut self,
        replacement: &Self::Ns,
        within: &Self::Ns,
    ) -> Result<(), anyhow::Error> {
        D::substitute(*self, replacement, within)
    }
}

#[test]
fn test_eq_stmt() {
    assert_parsed(&["x = y"], &["0 = 1"]);
}

#[test]
fn test_add_op() {
    assert_parsed(&["x = a + b - c"], &["+ 3 1 2", "- 5 3 4", "0 = 5"]);
}

#[test]
fn test_op_prec() {
    assert_parsed(
        &["x = a * b + c * d ^ e"],
        &["* 3 1 2", "^ 7 5 6", "* 8 4 7", "+ 9 3 8", "0 = 9"],
    );
}

#[test]
fn test_constant() {
    assert_parsed(&["x = 2"], &["1 == 2", "0 = 1"]);
}

#[test]
fn test_func_application() {
    assert_parsed(&["min(a, b) = log(a)"], &["min 2 0 1", "log 3 0", "2 = 3"]);
}

#[test]
fn test_implicit_application() {
    assert_parsed(&["y = lg x"], &["* 3 1 2", "0 = 3"]);
    assert_parsed(&["y = log x"], &["log 2 1", "0 = 2"]);
    assert_not_parsed(&["y = (log) x"]);
}

#[test]
fn test_definition_stmt() {
    assert_parsed(
        &["x = y", "f(x, y) := x + y"],
        &["0 = 1", "1:+ 1:2 1:0 1:1", "1:f := 1:2 1:0 1:1"],
    );
}

#[test]
fn test_substitiute_stmt() {
    assert_parsed(
        &["b^a => exp(log(b) * a)"],
        &[
            "1:^ 1:2 1:0 1:1",
            "1:2:log 1:2:3 1:2:0",
            "1:2:* 1:2:4 1:2:3 1:2:1",
            "1:2:exp 1:2:5 1:2:4",
            "1:2 = 1:2:5",
            "1 => 2",
        ],
    );
}

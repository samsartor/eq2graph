#![feature(drain_filter, map_first_last)]

use anyhow::Error;
use eq2graph::fromeq::Parser;
use eq2graph::graph::{Graph, NsId};
use eq2graph::sgi::GlasgowSolver;
use rustyline::{error::ReadlineError, Editor};
use std::fs;
use std::io::{stdout, BufWriter, Write};
use std::path::PathBuf;

#[derive(argh::FromArgs)]
/// Convert a system of equations to a graph
struct Eq2Graph {
    /// definitions to include before running the REPL
    #[argh(positional)]
    input: Option<PathBuf>,
    /// path to glasgow_subgraph_solver or equivalent
    #[argh(option)]
    glasgow: Option<PathBuf>,
}

#[cfg(feature = "minifb")]
fn open_dot(graph: &Graph) -> Result<(), Error> {
    use minifb::{Key, ScaleMode, Window, WindowOptions};
    use std::process::{Command, Stdio};

    let image = {
        let mut neato = Command::new("neato")
            .arg("-Tpng")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()?;

        graph.print_graphvis(BufWriter::new(neato.stdin.take().unwrap()))?;
        let output = neato.wait_with_output()?;
        image::load_from_memory_with_format(&output.stdout, image::ImageFormat::Png)?.into_rgb8()
    };

    let pixels: Vec<_> = image
        .pixels()
        .map(|&p| {
            let [r, g, b] = p.0.map(|x| x as u32);
            (r << 16) | (g << 8) | b
        })
        .collect();
    let width = image.width() as usize;
    let height = image.height() as usize;
    drop(image);

    let mut window = Window::new(
        "Eq2Graph",
        width,
        height,
        WindowOptions {
            resize: true,
            scale_mode: ScaleMode::Center,
            ..WindowOptions::default()
        },
    )
    .expect("Unable to open Window");

    while window.is_open() && !window.is_key_down(Key::Escape) {
        window.update_with_buffer(&pixels, width, height).unwrap();
    }

    Ok(())
}

#[cfg(not(feature = "minifb"))]
fn open_dot(_graph: &Graph) -> Result<(), Error> {
    use anyhow::bail;

    bail!("can not display graph: not compiled with minifb")
}

fn respond(mut line: String, graph: &mut Graph, output: impl Write, ns: NsId) -> Result<(), Error> {
    if let Some(var) = line.strip_prefix("solve ") {
        let var = Parser::new_in(&mut *graph, ns).parse_var(var)?;
        graph.print_equation(output, var);
    } else if &line == "dot" {
        open_dot(graph)?;
    } else if let Some(file) = line.strip_prefix("dot ") {
        graph.print_graphvis(BufWriter::new(fs::File::create(file)?))?;
    } else if let Some(file) = line.strip_prefix("csv ") {
        graph.print_csv(
            BufWriter::new(fs::File::create(file)?),
            |_, _| true,
            |_, _| true,
            |_, _| true,
        )?;
    } else {
        line.push('\n');
        Parser::new_in(&mut *graph, ns).parse_all(&line)?;
        graph.simplify();
    }

    Ok(())
}

fn main() -> Result<(), Error> {
    let Eq2Graph { input, glasgow } = argh::from_env();

    let mut graph = Graph::default();
    graph.define_defaults();

    if let Some(glasgow) = glasgow {
        graph.sgi_solver = Box::new(GlasgowSolver {
            command: glasgow.into_os_string(),
        });
    }

    if let Some(input) = input {
        let input = fs::read_to_string(input)?;
        graph.add_equations(&input)?;
        graph.simplify();
    }

    let output = stdout();
    let mut editor = Editor::<()>::new();

    let ns = graph.namespace(None);
    loop {
        let line = match editor.readline("[in] ") {
            Ok(l) => l,
            Err(ReadlineError::Eof | ReadlineError::Interrupted) => break,
            Err(e) => return Err(e.into()),
        };
        let mut output = output.lock();
        match respond(line, &mut graph, &mut output, ns) {
            Ok(_) => (),
            Err(e) => writeln!(output, "[err] {}", e)?,
        }
    }

    Ok(())
}

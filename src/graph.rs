mod dot;
mod internal;
pub mod norm;

pub use internal::*;

#[derive(Default, Clone, Debug)]
struct IdAlloc(usize);

impl IdAlloc {
    fn next(&mut self) -> usize {
        let next = self.0 + 1;
        std::mem::replace(&mut self.0, next)
    }
}

impl Graph {
    pub fn add_equations(&mut self, input: &str) -> Result<(), anyhow::Error> {
        crate::fromeq::Parser::new(self).parse_all(input)?;
        Ok(())
    }
}

impl<'a> crate::fromeq::Driver<'a> for Graph {
    type Var = VarId;
    type Ns = NsId;
    type Op = &'a str;

    fn namespace(&mut self, parent: Option<&Self::Ns>) -> Self::Ns {
        self.namespace(parent.copied())
    }

    fn operation(&self, op: &'a str) -> Option<Self::Op> {
        if norm::SUPPORTED_ALIASES.contains(&op) || self.is_defined(op) {
            Some(op)
        } else {
            None
        }
    }

    fn define<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &'a str,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b,
    {
        self.def(*ns, op, args.into_iter().copied().collect());
    }

    fn variable(&mut self, ns: &Self::Ns, name: Option<&'a str>) -> Self::Var {
        self.var(*ns, name)
    }

    fn merge(&mut self, var: &Self::Var, into: &Self::Var) {
        self.merge(*var, *into);
    }

    fn node<'b>(
        &mut self,
        ns: &Self::Ns,
        op: &Self::Op,
        args: impl IntoIterator<Item = &'b Self::Var>,
    ) where
        'a: 'b,
    {
        if let Err(NoSuchOp) = self.rel(*ns, op, args.into_iter().copied().collect()) {
            panic!("unsupported operation: {}", op);
        }
    }

    fn constant(&mut self, var: &Self::Var, value: i32) {
        self.set_constant(*var, Constant::Int(value));
    }

    fn substitute(
        &mut self,
        replacement: &Self::Ns,
        within: &Self::Ns,
    ) -> Result<(), anyhow::Error> {
        use std::collections::HashSet;

        let pattern = self.ref_namespace(*replacement).parent().unwrap();
        let mappings = self.sgi_solver.find(self, pattern, *within)?;

        for mapping in &mappings {
            let var_mapping = mapping.vars.iter().copied().collect();
            self.copy_from_ns(*replacement, *within, var_mapping);
        }

        let mut to_remove = HashSet::new();
        for mapping in mappings {
            for (_, cid) in mapping.consts {
                to_remove.insert(NodeId::Const(cid));
            }
            for (_, rid) in mapping.rels {
                to_remove.insert(NodeId::Rel(rid));
            }
        }
        self.remove_nodes(&to_remove);
        self.garbage_collect_vars();

        Ok(())
    }
}

use crate::graph;
use std::fmt::Display;
use std::io;

mod internal;
pub use internal::Printer;

impl graph::Graph {
    pub fn to_equation(&self, driver: impl Driver, starting_with: graph::VarId) {
        let mut printer = Printer::new(self, driver);
        printer.add_starting_point(starting_with);
        printer.print();
    }

    pub fn print_equation(&self, output: impl io::Write, starting_with: graph::VarId) {
        self.to_equation(DumbPrinter(output), starting_with);
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Operation {
    Add,
    Sub,
    Mul,
    Div,
    Pow,
    Root,
    Log,
    Pow2,
    Root2,
    PmRoot2,
    Exp,
    Ln,
}

impl Operation {
    pub fn as_symbol(self) -> &'static str {
        use Operation::*;

        match self {
            Add => "+",
            Sub => "-",
            Mul => "*",
            Div => "/",
            Pow => "^",
            Root => "root",
            Log => "log",
            Pow2 => "sq",
            Root2 => "sqrt",
            PmRoot2 => "ssqrt",
            Exp => "exp",
            Ln => "ln",
        }
    }

    pub fn from_const(con: &graph::Constant, output: usize) -> Option<Self> {
        use graph::Constant as Con;
        use Operation as Op;

        Some(match (con, output) {
            (Con::Add, 0) => Op::Add,
            (Con::Add, _) => Op::Sub,
            (Con::Mul, 0) => Op::Mul,
            (Con::Mul, _) => Op::Div,
            (Con::Pow, 0) => Op::Pow,
            (Con::Pow, 1) => Op::Root,
            (Con::Pow, 2) => Op::Log,
            (Con::Sq { signed: _ }, 0) => Op::Pow2,
            (Con::Sq { signed: false }, 1) => Op::Root2,
            (Con::Sq { signed: true }, 1) => Op::PmRoot2,
            (Con::Exp, 0) => Op::Exp,
            (Con::Exp, 1) => Op::Ln,
            _ => return None,
        })
    }
}

pub trait Driver {
    type Expr: Clone;

    fn constant(&mut self, value: impl Display) -> Self::Expr;
    fn variable(&mut self, id: impl Display, name: Option<&str>) -> Self::Expr;
    fn function(
        &mut self,
        name: &str,
        variation: impl Display,
        inputs: impl IntoIterator<Item = Self::Expr>,
    ) -> Self::Expr;
    fn operation(
        &mut self,
        operation: Operation,
        inputs: impl IntoIterator<Item = Self::Expr>,
    ) -> Self::Expr;
    fn equality(&mut self, left: Self::Expr, right: Self::Expr, def: bool);
}

pub struct DumbPrinter<W>(pub W);

impl<W: io::Write> Driver for DumbPrinter<W> {
    type Expr = String;

    fn constant(&mut self, value: impl Display) -> Self::Expr {
        value.to_string()
    }

    fn variable(&mut self, id: impl Display, name: Option<&str>) -> Self::Expr {
        if let Some(name) = name {
            name.to_owned()
        } else {
            format!("?{}", id)
        }
    }

    fn function(
        &mut self,
        name: &str,
        variation: impl Display,
        inputs: impl IntoIterator<Item = Self::Expr>,
    ) -> Self::Expr {
        let mut out = format!("{}_{}(", name, variation);
        for (i, input) in inputs.into_iter().enumerate() {
            if i != 0 {
                out.push_str(", ");
            }
            out.push_str(&input);
        }
        out.push(')');
        out
    }

    fn operation(
        &mut self,
        operation: Operation,
        inputs: impl IntoIterator<Item = Self::Expr>,
    ) -> Self::Expr {
        let mut out = format!("{}(", operation.as_symbol());
        for (i, input) in inputs.into_iter().enumerate() {
            if i != 0 {
                out.push_str(", ");
            }
            out.push_str(&input);
        }
        out.push(')');
        out
    }

    fn equality(&mut self, left: Self::Expr, right: Self::Expr, def: bool) {
        if def {
            let _ = writeln!(self.0, "{} := {}", left, right);
        } else {
            let _ = writeln!(self.0, "{} = {}", left, right);
        }
    }
}
